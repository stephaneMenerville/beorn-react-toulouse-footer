import React, {PropTypes} from 'react';
import {Grid, Button, ButtonGroup, ButtonToolbar} from "react-bootstrap"
import ThreeItemsLayoutMedia from 'components/layouts/three-items-layout-media'
import ThreeItemsLayoutContact from 'components/layouts/three-items-layout-contact'
import OneItemLayout from 'components/layouts/one-item-layout'

export default class Footer extends  React.Component {

  constructor(props) {
    super(props);
      this.state = {
        mode : "view"
      }
    }

  handleEdit() {
    this.setState(
      {
        mode : "edit"
      }
    )
  }

  handleSave() {
    this.setState(
      {
        mode : "view"
      }
    )
  }

  buildEditButton() {
    let name = "Edit"
    let handler = this.handleEdit.bind(this);
    if (this.state.mode == "edit"){
      name = "save";
      handler = this.handleSave.bind(this);
    }
    return (<Button onClick={handler}>{name}</Button>)
  }

  buildActionBar() {
    let actionBar;
    if(this.props.loggedIn) {
      actionBar = (
        <ButtonGroup>
          {this.buildEditButton()}
        </ButtonGroup>
      );
    }
    return actionBar;
  }

  render() {

    return (
      <Grid fluid className="footer">
        {this.buildActionBar()}
        <ThreeItemsLayoutMedia mode={this.state.mode}/>
        <ThreeItemsLayoutContact mode={this.state.mode} />
        <OneItemLayout mode={this.state.mode} />
      </Grid>
    );
  }
}

Footer.propTypes = {
  loggedIn: React.PropTypes.bool.isRequired
};
