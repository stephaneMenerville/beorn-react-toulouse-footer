import React, {PropTypes} from 'react';

export default class BaseLayout extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>{this.props.children}</div>
    );
  }
}

BaseLayout.propTypes = {
};
