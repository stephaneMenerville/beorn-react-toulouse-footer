import React, { PropTypes } from 'react'
import SquareContent from 'components/widgets/square-content'
import Video from 'components/widgets/video'
import {Row,Col} from 'react-bootstrap'

class ThreeItemsLayoutMedia extends React.Component {
  render () {

    let article1 = [
        <section className="gallery-image">
          <h3>Photos</h3>
          <a href="" target="_blank" title="Flickr Toulouse">
            <img src="http://toulouse.fr/toulouse-theme/images/instagram-toulouse.jpg" alt="Flickr Toulouse"></img>
          </a>
          <div className="link-instagram">
            <a href="https://www.flickr.com/photos/toulousefr/albums" target="_blank" title="Instagram Toulouse"> Toutes les images </a>
          </div>
        </section>

    ]

    let article2 = [
      <section className="content-footer-3">
        <h2> Publications </h2>
        <ul className="image-pub">
          <div className="pub-1">
            <li className="child-menu">
              <a href="http://toulouse.fr/web/publications/a-toulouse">
                <img src="http://toulouse.fr/documents/10192/280573/a-toulouse.jpg/cbf8f1be-056f-4abc-8a74-e3e4b15b86af?t=1363342247196" alt="A toulouse" title="A toulouse"> </img>
              </a>
            </li>
          </div>
          <li className="child-menu">
            <a href="http://www.toulouse.fr/web/publications/la-lettre-de-la-cms">
              <img src="http://toulouse.fr/documents/10192/297089/lettreCMS.jpg/fc72785b-015b-4ad4-8d3f-799cca2b200d?t=1385375640607" alt="Lettre de la Communauté municipale de santé" title="Lettre de la Communauté municipale de santé"> </img>
            </a>
          </li>
          <li className="child-menu">
            <a href="http://www.toulouse.fr/web/publications/a-toulouse/hors-series">
              <img src="http://toulouse.fr/documents/10192/297089/VisuelHorsSeries.jpg/3f09fd4d-543f-473a-ade3-476a06a1e001?t=1400833477269" alt="Hors séries" title="Hors séries"> </img>
            </a>
          </li>
          <li className="child-menu">
            <a href="http://www.cultures.toulouse.fr/kiosque">
              <img src="http://toulouse.fr/documents/10192/280573/centre-culturel.jpg/71d8d36b-166f-42c7-819d-392c62bcec35?t=1363342247251" alt="Centres culturels" title="Centres culturels"> </img>
            </a>
          </li>
        </ul>
      </section>
    ]

    let video = [
      <div>
        <h3>Vidéos</h3>
        <div className="agregator-styles">
          <article className="big-article" style={{}}>
            <a href="http://www.toulouse.fr/web/videos/-/toulouse-plages-2016-un-plancher-de-danse?redirect=%2F" className="">
              <img src="http://toulouse.fr/toulouse-theme/images/bg-video.png" className="article-image" alt="image extraite de la vidéo" title="image extraite de la vidéo Toulouse plages 2016. Alors on danse ?"></img>
            </a>
          </article>
          <article className="big-article" style={{}}>
            <a href="http://www.toulouse.fr/web/videos/-/toulouse-plages-2016-un-plancher-de-danse?redirect=%2F" className="">
              <img src="http://toulouse.fr/toulouse-theme/images/bg-video.png" className="article-image" alt="image extraite de la vidéo" title="image extraite de la vidéo Toulouse plages 2016. Alors on danse ?"></img>
            </a>
          </article>
          <article className="big-article" style={{}}>
            <a href="http://www.toulouse.fr/web/videos/-/toulouse-plages-2016-un-plancher-de-danse?redirect=%2F" className="">
              <img src="http://toulouse.fr/toulouse-theme/images/bg-video.png" className="article-image" alt="image extraite de la vidéo" title="image extraite de la vidéo Toulouse plages 2016. Alors on danse ?"></img>
            </a>
          </article>
          <article className="big-article" style={{}}>
            <a href="http://www.toulouse.fr/web/videos/-/toulouse-plages-2016-un-plancher-de-danse?redirect=%2F" className="">
              <img src="http://toulouse.fr/toulouse-theme/images/bg-video.png" className="article-image" alt="image extraite de la vidéo" title="image extraite de la vidéo Toulouse plages 2016. Alors on danse ?"></img>
            </a>
          </article>
        </div>
        <div className="link-video">
          <a href="/web/videos" title="Videos">Toutes les vidéos</a>
        </div>
      </div>    
    ]


    return (
      <Row className="content-footer media">
        <Col sm={12}>
          <h2>Toulouse Média</h2>
        </Col>
        <Col sm={4}>
          <SquareContent mode={this.props.mode} content={article1} />
        </Col>
        <Col sm={4}>
          <Video mode={this.props.mode} content={video} />
        </Col>
        <Col sm={4}>
          <SquareContent mode={this.props.mode} content={article2}/>
        </Col>
      </Row>
    )

  }
}

ThreeItemsLayoutMedia.propTypes = {
  mode: React.PropTypes.string.isRequired
};
export default ThreeItemsLayoutMedia;
