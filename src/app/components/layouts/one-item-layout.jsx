import React, { PropTypes } from 'react'
import SiteMap from 'components/widgets/site-map'
import {Row,Col} from 'react-bootstrap'

class OneItemLayout extends React.Component {

  render () {
    let article5 = [
      <div>
        <h2>
          <p>
            <span>Mairie de Toulouse - 1 place du Capitole, 31000 Toulouse.&nbsp; |</span>
            <a href=""> Adresses des mairies de quartier</a>
            <br></br>
            <span>Standard tous services, 7j/7 et 24h/24&nbsp; : 05 61 22 29 22 </span>
          </p>
        </h2>
        <div className="footer-center">
          <ul className="sub-menu">
            <div className="separate-li">
              <li className="child-menu">
                <a href="/web/guest/en-un-clic"> En un clic </a>
                <ul>
                  <li>
                    <span>›</span>
                    <a href="/web/guest/contacts-utiles"> Contacts utiles </a>
                  </li>
                  <li>
                    <span>›</span>
                    <a href="/web/guest/en-un-clic/les-plus-consultees"> Les plus consultées </a>
                  </li>
                </ul>
              </li>
            </div>
          </ul>
        </div>
      </div>
    ]
    return (
      <Row className="content-footer site-map">
        <Col sm={12}>
          <SiteMap mode={this.props.mode} content={article5}/>
        </Col>
      </Row>
    )

  }
}

OneItemLayout.propTypes = {
  mode: React.PropTypes.string.isRequired
};

export default OneItemLayout;
