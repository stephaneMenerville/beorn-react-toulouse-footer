import React, { PropTypes } from 'react'
import BezelContent from 'components/widgets/bezel-content'
import Newsletter from 'components/widgets/newsletter'
import {Row,Col} from 'react-bootstrap'

class ThreeItemsLayoutContact extends React.Component {
  render () {

    let newsletter = [
      <div>
        <h1 className="newsletter-title">Inscription aux newsletters</h1>
        <div>
          <input type="text" placeholder="Saisissez votre e-mail" className="mail-address field-required field-email"></input>
          <button className="btn newsletter-subscribe btn-primary" type="submit" disabled=""> OK </button>
        </div>
        <div className="newsletter-link">
          <a href="/web/newsletters" title="Newsletters"><span>›</span> Toutes les newsletters </a>
        </div>
      </div>
    ]

    let article3 = [
      <div>
        <article className="usefull-link">
          <h2> Une question </h2>
          <ul className="other-link">
            <li class="child-menu">
              <a href="http://toulouse.fr/web/guest/en-un-clic/faq">
                <span>›</span> Consultez notre question / réponse
              </a>
            </li>
            <li className="child-menu">
              <a href="http://toulouse.fr/web/guest/contact">
                <span>›</span> Contactez-nous par courriel
              </a>
            </li>
            <li className="child-menu">
              <a href="https://twitter.com/Toulouse">
                <span>›</span> Sur twitter @toulouse
              </a>
            </li>
          </ul>
        </article>
        <div className="question-number"></div>
      </div>
    ]

    let article4 = [
      <div>
        <article className="usefull-link">
          <h2>  Applications mobiles  </h2>
          <ul className="other-link">
            <li class="child-menu">
              <a href="http://toulouse.fr/web/guest/en-un-clic/faq">
                <span>›</span>  Bibli&co, bibliothèque de Toulouse
              </a>
            </li>
            <li className="child-menu">
              <a href="http://toulouse.fr/web/guest/contact">
                <span>›</span>  Urban-Hist
              </a>
            </li>
            <li className="child-menu">
              <a href="https://twitter.com/Toulouse">
                <span>›</span>  Chefs-dœuvre du musée des Augustins
              </a>
            </li>
            <li className="child-menu">
              <a href="https://twitter.com/Toulouse">
                <span>›</span>   Application du musée Les Abattoirs
              </a>
            </li>
          </ul>
        </article>
      </div>
    ]

    return (
      <Row className="content-footer">
        <Col sm={12}>
          <h2>Toulouse & Vous</h2>
        </Col>
        <Col sm={4}>
          <BezelContent mode={this.props.mode} content={article3}/>
        </Col>
        <Col sm={4}>
          <Newsletter mode={this.props.mode} content={newsletter}/>
        </Col>
        <Col sm={4}>
          <BezelContent mode={this.props.mode} content={article4}/>
        </Col>
      </Row>
    )


  }
}

ThreeItemsLayoutContact.propTypes = {
  mode: React.PropTypes.string.isRequired
};

export default ThreeItemsLayoutContact;
