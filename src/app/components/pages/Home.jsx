import React, {PropTypes} from 'react';
import {Button} from 'react-bootstrap'
import Footer from 'components/footer'

export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Footer loggedIn={true}/>
      </div>

  );
  }
}

Home.propTypes = {
};
