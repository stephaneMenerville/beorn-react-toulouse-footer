import React, { PropTypes } from 'react'
import StateComponent from 'components/widgets/state-component'

class Newsletter extends StateComponent {
  renderComponent(){
    return (
      <div>
        {this.props.content}
      </div>
    )

  }
}

export default Newsletter;
