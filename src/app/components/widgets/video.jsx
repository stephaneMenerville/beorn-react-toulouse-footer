import React, { PropTypes } from 'react'
import StateComponent from 'components/widgets/state-component'

class Video extends StateComponent {
  renderComponent () {
    return (
      <div>
        {this.props.content}
      </div>
    )
  }
}

export default Video;
