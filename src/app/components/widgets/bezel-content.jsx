import React, { PropTypes } from 'react'
import EditableContent from 'components/widgets/editable-content'

class BezelContent extends EditableContent {
  renderComponent(){
    return (
      <div>{this.props.content}</div>
    )
  }
}

export default BezelContent;
