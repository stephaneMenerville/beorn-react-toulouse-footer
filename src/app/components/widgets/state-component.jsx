import React, { PropTypes } from 'react'

class StateComponent extends React.Component {

  renderComponent(){
    return (
      <div>

      </div>
    )
  }

  render () {
    let bc = this.props.mode == "edit" ? "red" : "";
    let bs = this.props.mode == "edit" ? "solid" : "";
    let style = {
      active: {
        borderWidth: .5,
        borderStyle: bs,
        borderColor: bc,
        padding: 5
      }
    }

    return (
      <div style={style.active}>
          {this.renderComponent()}
      </div>
    )
  }
}

StateComponent.propTypes = {
  mode: React.PropTypes.string.isRequired,
  content : React.PropTypes.array
};

export default StateComponent;
