import React, { PropTypes } from 'react'
import StateComponent from 'components/widgets/state-component'
import AlloyEditor from 'alloyeditor'
import _ from "lodash"

class EditableContent extends StateComponent {
  constructor(props){
      super(props);
      this.state = {
        containerId: "",
        content: ""
      }
  }

  componentWillMount() {
    let containerId = _.uniqueId('block_');
    this.state = {
      containerId: containerId
    }
    this.setState(this.state)
  }

  componentWillUnmount() {
    if (this._editor) {
      this._editor.destroy()
    }
  }

  componentDidUpdate() {
    console.log('CDU');
    let contentId = this.state.containerId + "_content"
    if(this.props.mode == "edit"){
      this._editor = AlloyEditor.editable(contentId);
    } else {
      this._editor.destroy()
    }
  }

  renderEditableComponent(){
    if(this.props.mode == "edit") {
      console.log('REC SAVE');
      return (
        <div id={this.state.containerId}>
          <div id={this.state.containerId + "_content"}>
            {this.renderComponent()}
          </div>
        </div>
      )
    } else {
      console.log('REC VIEW');
      return (
        <div>
          {this.renderComponent()}
        </div>
      )
    }
  }

  render () {
    let bc = this.props.mode == "edit" ? "red" : "";
    let bs = this.props.mode == "edit" ? "solid" : "";
    let style = {
      active: {
        borderWidth: .5,
        borderStyle: bs,
        borderColor: bc,
        padding: 5
      }
    }
    return (
      <div style={style.active}>
          {this.renderEditableComponent()}
      </div>
    )
  }

}

EditableContent.propTypes = {
  mode: React.PropTypes.string.isRequired,
  content : React.PropTypes.array
};

export default EditableContent;
