import conf from '../../src/app/conf'

describe('Testing configuration setup', () => {
  it('should present a configuration file', () => {
    expect(conf).to.be.exist;
  })
})
