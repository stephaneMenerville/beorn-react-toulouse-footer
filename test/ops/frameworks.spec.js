import Route from '../../src/app/routes/routes'

describe('Testing Dev Ops Setup', () => {
  it('should expose the Chai expect method', () => {
    expect(1).to.not.equal(2);
  })
  it('should work with ES6 import export', () => {
    expect(Route).to.be.exist;
  })
})
