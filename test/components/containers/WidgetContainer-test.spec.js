import React from "react";
import ReactDOM from "react-dom";
import TestUtils from "react-addons-test-utils";
import {isComponentOfType} from 'react-shallow-testutils';

import WidgetContainer from "../../../src/app/components/containers/WidgetContainer"

describe('Testing WidgetContainer component', () => {
  var renderer, widgetContainer;
  beforeEach(function(){
    renderer = TestUtils.createRenderer();
    renderer.render(
      <WidgetContainer
        loggedIn={true}
      />
    );
    widgetContainer = renderer.getRenderOutput();
  });

  it("WidgetContainer should import react-bootstrap ", function(){
    expect(widgetContainer.props.componentClass).to.equal('div');
  });

  it("WidgetContainer wrapper should have the correct className ", function(){
   expect(widgetContainer.props.className).to.equal("widgetContainer");
   console.log('WidgetContainer className ', widgetContainer.props.className);
  });

  it("WidgetContainer component should display a configuration toolbar if connected", function(){

  });

  it("WidgetContainer should display a list of components", function(){

  });

})
