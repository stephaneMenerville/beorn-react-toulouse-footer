var gulp = require('gulp');
var gutil = require("gulp-util");
var path = require('path');
var envify = require('envify/custom');
var print = require('gulp-print');
var karma = require('karma').Server;


// Set variable via gulp --type production
var environment = gutil.env.type || 'development';
gutil.log("Building for environment", JSON.stringify(environment))
var isProduction = environment === 'production';

var runSequence = require('run-sequence')
var browserSync = require('browser-sync')
var browserify = require('browserify')
var watchify = require('watchify')
var notify = require('gulp-notify')
var source = require('vinyl-source-stream')
var buffer = require('vinyl-buffer')
var sourcemaps = require('gulp-sourcemaps')
var es = require('event-stream')
var debug = require('gulp-debug')

const  bowerDir = "./bower_components";


gulp.task('browserSync', () => {
  browserSync({
    server: {
      baseDir: './public',
    },
    port: global.port
  });
});

function getOptions(entry) {
  return {
    entries: [path.resolve("src", entry)],
    extensions: ['.jsx','.js'],
    paths: ['./node_modules', './src/app'],
    debug: ! isProduction
  };
}

function build(entry, bundler) {
    function rebundle() {
      return bundler.bundle()
        .on('error', notify.onError())
        .pipe(source(entry))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("public/js"))
        .pipe(browserSync.reload({stream: true}));
    }

    bundler
    .transform("babelify", {presets: ["react","es2015"]})
    .transform('node-lessify')
    .transform(envify({
        NODE_ENV: environment
      }))
    .on('update', rebundle)
    .on('log', function(msg){gutil.log(msg)})
    return rebundle();
}

gulp.task('images', function() { 
    return gulp.src('./src/app/assets/images/**.*') 
        .pipe(gulp.dest('./public/images')); 
});

gulp.task('icons', function() { 
    return gulp.src(bowerDir + '/font-awesome/fonts/**.*') 
        .pipe(gulp.dest('./public/fonts')); 
});

gulp.task('watchify', () => {
  // map them to our stream function
  var tasks = global.entries.map(function(entry) {
    var customOpts = getOptions(entry)
    var opts = Object.assign({}, watchify.args, customOpts);
    var bundler = watchify(browserify(opts), {poll: true});

    return build(entry, bundler)
  });

  // create a merged stream
  return es.merge.apply(null, tasks);
});

gulp.task('browserify', () => {
  // map them to our stream function
  var tasks = global.entries.map(function(entry) {
    var bundler = browserify(getOptions(entry))

    return build(entry, bundler)
  });

  // create a merged stream
  return es.merge.apply(null, tasks);
});

gulp.task('copy-alloyeditor', function() {
    return gulp.src(['./node_modules/alloyeditor/dist/alloy-editor/**/*'])
        .pipe(gulp.dest('./public/alloyeditor'));
});

gulp.task('test', function(done) {
    karma.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, function() {
        done;
    });
});

gulp.task('tdd', function(done) {
    karma.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: false
    }, function() {
        done;
    });
});

gulp.task('default', cb => {
  runSequence(['icons','images','browserSync', 'watchify'], cb);
});

gulp.task('build', cb => {
  runSequence(['icons','images','browserify'], cb);
});
